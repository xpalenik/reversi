package reversi;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VyberVelkostiHracejPlochy implements ActionListener {
    final JFrame f;
    Integer rozmer = null;

    VyberVelkostiHracejPlochy() {
        f = new JFrame("Velkost hracej plochy");
        f.setLayout(new FlowLayout());

        JLabel vyzva = new JLabel("Vyberte prosim velkost hracej plochy: ", JLabel.CENTER);
        vyzva.setFont(new Font("Arial", Font.PLAIN, 22));
        f.add(vyzva);

        Integer[] boardSizes = {6, 8, 10, 12};
        JComboBox<Integer> cb = new JComboBox<>(boardSizes);
        cb.setSelectedItem(null);
        cb.setFont(new Font("Arial", Font.BOLD, 40));
        cb.addActionListener(this);
        f.add(cb);

        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox<Integer> cb = (JComboBox<Integer>)e.getSource();
        this.rozmer = (Integer)cb.getSelectedItem();

        f.setVisible(false);
        f.dispose();
        new ReversiJFrame(rozmer);
    }
}