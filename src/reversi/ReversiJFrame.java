package reversi;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.*;

public class ReversiJFrame extends JFrame {

    public ReversiJFrame(int rozmer) {
        super("Reversi");

        setLayout(new BorderLayout());
        add(new ReversiJPanel(rozmer), BorderLayout.CENTER);
        this.pack();
        setLocationRelativeTo (null);
        setResizable(false);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

        KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false);
        KeyStroke restartKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_R, 0, false);

        Action escapeAction = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                ReversiJFrame.this.dispose();
            }
        };

        Action restartAction = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ReversiJFrame.this.dispose();
                new VyberVelkostiHracejPlochy();
            }
        };

        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "ESCAPE");
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(restartKeyStroke, "R");

        getRootPane().getActionMap().put("ESCAPE", escapeAction);
        getRootPane().getActionMap().put("R", restartAction);
    }

    public static void main(String[] args) {
        new ReversiJFrame(6);
    }
}

    
