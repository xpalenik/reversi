package reversi;

class ZvyraznenePole {
    private int stlpec = -1;
    private int riadok = -1;

    public boolean isSetTo(int stlpec, int riadok){
        return this.stlpec == stlpec && this.riadok == riadok;
    }

    public boolean isSet(){
        return  stlpec !=-1 && riadok !=-1;
    }

    public void unset(){
        stlpec = -1;
        riadok = -1;
    }

    public void set(int stlpec, int riadok){
        this.stlpec = stlpec;
        this.riadok = riadok;
    }

    public int getStlpec() {
        return stlpec;
    }

    public int getRiadok() {
        return riadok;
    }
}
