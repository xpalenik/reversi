package reversi;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ThreadLocalRandom;
import javax.imageio.ImageIO;
import javax.swing.*;

import static java.lang.Math.min;

public class ReversiJPanel extends JPanel {

    final Color farbaPozadia = new Color(39, 223, 197);
    final Color farbaZvyraznenia = new Color(232, 203, 46, 100);
    final Color farbaZvyrazneniaTahuPocitaca = new Color(255, 0, 0, 75);

    private final int ROZMER;
    final Kamen[][] policka;

    private final ZvyraznenePole zvyraznenePole = new ZvyraznenePole();
    private final ZvyraznenePole zvyraznenePoleTahuPocitaca = new ZvyraznenePole();

    private int prevedXnaStlpec(final int x){
        return min(x / (hraciaPlocha.getWidth()/ROZMER), ROZMER-1);
    }

    private int prevedYnaRiadok(final int y){
        return min(y / (hraciaPlocha.getHeight()/ROZMER), ROZMER-1);
    }

    final JLabel lblAktualnyHrac;
    final JButton btnPrenechajTah;
    final JButton btnNahodnyTah;
    final JPanel hraciaPlocha;

    private Kamen naTahuJe;

    int skoreCierneho = 2, skoreBieleho = 2;

    public ReversiJPanel(int rozmer) {
        super(new BorderLayout());

        this.ROZMER = rozmer;
        this.policka = new Kamen[ROZMER][ROZMER];

        // INFORMACNY PANEL (naspodku)

        JPanel infoPanel = new JPanel(new GridLayout());

        lblAktualnyHrac = new JLabel("  ťahá: ČIERNY (hráč)", JLabel.LEFT);
        lblAktualnyHrac.setFont(new Font("Arial", Font.BOLD, 24));

        String textRozmery = "rozmer (" + ROZMER + " X " + ROZMER + ") ";
        JLabel rozmery = new JLabel(textRozmery, JLabel.RIGHT);
        rozmery.setFont(new Font("Arial", Font.BOLD, 24));

        infoPanel.add(lblAktualnyHrac);
        infoPanel.add(rozmery);
        add(infoPanel, BorderLayout.SOUTH);

        // HRACIA PLOCHA (stred)

        hraciaPlocha = new JPanel(new GridLayout(ROZMER,ROZMER));
        for(int riadok = 0; riadok < ROZMER; riadok++) {
            for(int stlpec=0; stlpec < ROZMER; stlpec++) {
                JPanel stvorcek = new JPanel(new BorderLayout());
                stvorcek.setBackground(farbaPozadia);
                stvorcek.setBorder(BorderFactory.createLineBorder(Color.gray));
                hraciaPlocha.add(stvorcek);
            }
        }
        add(hraciaPlocha, BorderLayout.CENTER);

        // OVLADACI PANEL (hore)

        JPanel ovladaciPanel = new JPanel(new FlowLayout());

        JButton btnNovaHra = new JButton("nová hra");
        btnNovaHra.setFont(new Font("Arial", Font.BOLD, 18));
        btnNovaHra.addActionListener(e -> stlacTlacitkoNovaHra());

        btnPrenechajTah = new JButton("prenechaj ťah");
        btnPrenechajTah.setFont(new Font("Arial", Font.BOLD, 18));
        btnPrenechajTah.addActionListener(e -> zmenaTahu());

        btnNahodnyTah = new JButton("zahraj ťah počítača");
        btnNahodnyTah.setFont(new Font("Arial", Font.BOLD, 18));
        btnNahodnyTah.addActionListener(e -> {
            if (zvyraznenePoleTahuPocitaca.isSet()) {
                zafarbi(zvyraznenePoleTahuPocitaca.getStlpec(), zvyraznenePoleTahuPocitaca.getRiadok(), farbaPozadia);
                zvyraznenePoleTahuPocitaca.unset();
            }

            ArrayList<Integer> stlpceMoznychTahov = new ArrayList<>();
            ArrayList<Integer> riadkyMoznychTahov = new ArrayList<>();

            for (int riadok = 0; riadok < ROZMER; riadok++){
                for (int stlpec = 0; stlpec < ROZMER; stlpec++){
                    if (zajatKamene(stlpec, riadok, naTahuJe, false)){
                        stlpceMoznychTahov.add(stlpec);
                        riadkyMoznychTahov.add(riadok);
                    }
                }
            }

            if (stlpceMoznychTahov.size() < 1) {
                zmenaTahu();
                return;
            }

            int indexNahodnehoTahu = ThreadLocalRandom.current().nextInt(0, stlpceMoznychTahov.size());
            int stlpecNahodneho = stlpceMoznychTahov.get(indexNahodnehoTahu);
            int riadokNahodneho = riadkyMoznychTahov.get(indexNahodnehoTahu);

            zajatKamene(stlpecNahodneho, riadokNahodneho, naTahuJe, true);
            umiestniKamen(stlpecNahodneho, riadokNahodneho, naTahuJe);

            zvyraznenePoleTahuPocitaca.set(stlpecNahodneho, riadokNahodneho);
            zafarbi(stlpecNahodneho, riadokNahodneho, farbaZvyrazneniaTahuPocitaca);

            update();

            zmenaTahu();
        });
        btnNahodnyTah.setVisible(false);

        ovladaciPanel.add(btnNovaHra);
        ovladaciPanel.add(btnPrenechajTah);
        ovladaciPanel.add(btnNahodnyTah);

        add(ovladaciPanel, BorderLayout.NORTH);

        novaHra();
    }

    private void zafarbi(int stlpec, int riadok, Color farba) {
        JPanel panel = (JPanel) hraciaPlocha.getComponent(prevedNaIndex(stlpec, riadok));
        panel.setBackground(farba);
    }

    public void novaHra() {
        zmazatZvyraznenie();

        for(int riadok = 0; riadok < ROZMER; riadok++) { for(int stlpec = 0; stlpec < ROZMER; stlpec++) { policka[riadok][stlpec] = Kamen.ZIADNY;}}
        naTahuJe = Kamen.CIERNY;
        umiestniKamen(ROZMER/2-1,ROZMER/2-1, Kamen.BIELY);
        umiestniKamen(ROZMER/2-1,ROZMER/2, Kamen.CIERNY);
        umiestniKamen(ROZMER/2,ROZMER/2, Kamen.BIELY);
        umiestniKamen(ROZMER/2,ROZMER/2-1, Kamen.CIERNY);
        update();

        pridajListenery();
    }

    private void pridajListenery(){
        hraciaPlocha.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                zmazatZvyraznenie();

                int stlpec = prevedXnaStlpec(e.getX());
                int riadok = prevedYnaRiadok(e.getY());

                if(zajatKamene(stlpec, riadok, naTahuJe, false)) {
                    zajatKamene(stlpec, riadok, naTahuJe, true);
                    umiestniKamen(stlpec, riadok, naTahuJe);
                    update();
                    zmenaTahu();
                }
            }
            public void mousePressed(MouseEvent e) {}
            public void mouseReleased(MouseEvent e) {}
            public void mouseEntered(MouseEvent e) {}
            public void mouseExited(MouseEvent e) {}
        });

        hraciaPlocha.addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent mouseEvent) {
            }

            @Override
            public void mouseMoved(MouseEvent mouseEvent) {
                Point p = mouseEvent.getPoint();

                try {
                    int stlpec = prevedXnaStlpec(p.x);
                    int riadok = prevedYnaRiadok(p.y);

                    if (!zvyraznenePole.isSetTo(stlpec, riadok)){
                        zmazatZvyraznenie();
                    }

                    if(zajatKamene(stlpec, riadok, naTahuJe, false)) {
                        zvyraznenePole.set(stlpec, riadok);
                        JPanel panel = (JPanel) hraciaPlocha.getComponent(prevedNaIndex(stlpec, riadok));
                        panel.setBackground(farbaZvyraznenia);
                    }

                    update();

                } catch (IndexOutOfBoundsException e) {
                    System.out.println(e.getMessage());
                }
            }
        });
    }

    private void odstranListenery(){
        for (MouseListener listener : hraciaPlocha.getMouseListeners()){
            hraciaPlocha.removeMouseListener(listener);
        }

        for (MouseMotionListener listener : hraciaPlocha.getMouseMotionListeners()){
            hraciaPlocha.removeMouseMotionListener(listener);
        }
    }

    private void stlacTlacitkoNovaHra(){
        JFrame parent = (JFrame) this.getTopLevelAncestor();
        parent.dispose();

        new VyberVelkostiHracejPlochy();
    }

    private void zmazatZvyraznenie() {
        if (this.zvyraznenePole.isSet()){
            JPanel panel = (JPanel) hraciaPlocha.getComponent(prevedNaIndex(this.zvyraznenePole.getStlpec(), this.zvyraznenePole.getRiadok()));
            panel.setBackground(farbaPozadia);
            zvyraznenePole.unset();
        }

        if (zvyraznenePoleTahuPocitaca.isSet()){
            zafarbi(zvyraznenePoleTahuPocitaca.getStlpec(), zvyraznenePoleTahuPocitaca.getRiadok(), farbaPozadia);
            zvyraznenePoleTahuPocitaca.unset();
        }
    }

    public void umiestniKamen(int stlpec, int riadok, Kamen farba) {
        if(policka[stlpec][riadok] == Kamen.ZIADNY)
            policka[stlpec][riadok] = farba;
    }

    public void update() {
        skoreCierneho = 0; skoreBieleho = 0;
        for(int riadok = 0; riadok < ROZMER; riadok ++) { for(int stlpec = 0; stlpec < ROZMER; stlpec++) { JPanel panel = (JPanel) hraciaPlocha.getComponent(prevedNaIndex(stlpec , riadok)); panel.removeAll();}}
        for(int stlpec = 0; stlpec < ROZMER; stlpec ++) {
            for(int riadok = 0; riadok < ROZMER; riadok++) {
                if(zajatKamene(stlpec, riadok, naTahuJe, false))
                    pridajObrazok(stlpec, riadok, Kamen.PRIEHLADNY.toString().toLowerCase());
                
                if(policka[stlpec][riadok] == Kamen.CIERNY) {
                    pridajObrazok(stlpec, riadok, Kamen.CIERNY.toString().toLowerCase());
                    skoreCierneho++;
                }
                 if(policka[stlpec][riadok] == Kamen.BIELY) {
                    pridajObrazok(stlpec, riadok, Kamen.BIELY.toString().toLowerCase());
                    skoreBieleho++;
                }          
                skontrolujCiJeKoniecHry(skoreCierneho, skoreBieleho);
            }
        }
    }

    private boolean jeNaHracejPloche(int stlpec, int riadok) {
        return riadok >= 0 && stlpec >= 0 && riadok < ROZMER && stlpec < ROZMER;
    }

    private boolean jeOpacnejFarby(int stlpec, int riadok, Kamen farba) {
        return policka[riadok][stlpec] == (farba == Kamen.CIERNY ? Kamen.BIELY : Kamen.CIERNY);
    }

    public boolean zajatKamene(int stlpec, int riadok, Kamen naTahuJe, boolean zajatKamene) {

        if (policka[stlpec][riadok] != Kamen.ZIADNY)
            return false;

        boolean zajaliSmeKamene = false;
        for(int smerX = -1; smerX < 2; smerX++) {
            for(int smerY = -1; smerY < 2; smerY ++) {
                int stlpecVsmereX = stlpec + smerX;
                int riadokVsmereY = riadok + smerY;

                if((smerX == 0 && smerY == 0) || !jeNaHracejPloche(riadokVsmereY, stlpecVsmereX) || !jeOpacnejFarby(riadokVsmereY, stlpecVsmereX, naTahuJe))
                    continue;

                for(int vzdialenostKamenaRovnakejFarby = 0; vzdialenostKamenaRovnakejFarby < ROZMER; vzdialenostKamenaRovnakejFarby++) {
                    int stlpecRovnakejFarby = stlpec+vzdialenostKamenaRovnakejFarby*smerX;
                    int riadokRovnakejFarby = riadok+vzdialenostKamenaRovnakejFarby*smerY;

                    if(!jeNaHracejPloche(riadokRovnakejFarby, stlpecRovnakejFarby))
                        break;

                    if(policka[stlpecRovnakejFarby][riadokRovnakejFarby] == naTahuJe) {
                        if(zajatKamene) {
                            for(int vzdialenostKamenaOdlisnejFarby = 1; vzdialenostKamenaOdlisnejFarby < vzdialenostKamenaRovnakejFarby; vzdialenostKamenaOdlisnejFarby ++) {
                                int stlpecCoZmeniFarbu = stlpec+vzdialenostKamenaOdlisnejFarby*smerX;
                                int riadokCoZmeniFarbu = riadok+vzdialenostKamenaOdlisnejFarby*smerY;
                                policka[stlpecCoZmeniFarbu][riadokCoZmeniFarbu] = naTahuJe;
                            }
                        }

                        zajaliSmeKamene = true;
                        break;
                    }
                }
            }
        }
        return zajaliSmeKamene;
    }

    public void pridajObrazok(int stlpec, int riadok, String farba) {
        try {
            BufferedImage obrazok = ImageIO.read(Objects.requireNonNull(ReversiJPanel.class.getClassLoader().getResourceAsStream(farba + ".png")));
            JLabel picLbl = new JLabel(new ImageIcon(obrazok));
            JPanel panel = (JPanel) hraciaPlocha.getComponent(prevedNaIndex(stlpec, riadok));
            panel.removeAll();
            panel.add(picLbl);
            hraciaPlocha.updateUI();
        } catch (IOException e ) {
            System.out.println("Nepodarilo sa nacitat obrazky kamenov");
            e.printStackTrace();
        }
    }

    private void zmenaTahu() {
        naTahuJe = (naTahuJe == Kamen.CIERNY ? Kamen.BIELY : Kamen.CIERNY);

        if (naTahuJe == Kamen.CIERNY) {
            btnNahodnyTah.setVisible(false);
            btnPrenechajTah.setVisible(true);
            pridajListenery();
        } else {
            btnNahodnyTah.setVisible(true);
            btnPrenechajTah.setVisible(false);
            odstranListenery();
        }

        update();
        lblAktualnyHrac.setText("  ťahá: " + (naTahuJe == Kamen.CIERNY ? "ČIERNY (hráč)" : "BIELY (PC)"));
    }

    private int prevedNaIndex(int stlpec, int riadok) {
        return (riadok * ROZMER) + stlpec;
    }

    private void skontrolujCiJeKoniecHry(int ciernychKamenov, int bielychKamenov) {
        if(ciernychKamenov + bielychKamenov == ROZMER*ROZMER){
            String skore = "\nčierny: " + ciernychKamenov + " biely: " + bielychKamenov;
            if (ciernychKamenov > bielychKamenov) vypisSpravu("vyhral ČIERNY" + skore);
            if (ciernychKamenov < bielychKamenov) vypisSpravu("vyhral BIELY" + skore);
            if (ciernychKamenov ==bielychKamenov) vypisSpravu("remíza");
            novaHra();
        }
    }

    private void vypisSpravu(String sprava) {
        JOptionPane.showMessageDialog(null, sprava, "Koniec hry", JOptionPane.INFORMATION_MESSAGE);
    }
}
